import { Data } from 'metalthea'
import { setupData } from './setup'

export const audio: Data.ExecutionContext = {}

audio.playSound = function (sound) {
  return `${setupData.soundSynthName}(${sound})`
}

audio.playMusic = function (music) {
  return `${setupData.musicPlayerName}(${music})`
}
