import { Data } from 'metalthea'
import { deepJoin } from 'utils/data-structure'
/** data execution context */
export const data: Data.ExecutionContext = {}
// TODO: add, sub, etc. + group('()') + const like undefined(safe?), Infinity(safe?), Pi(accurate?), random(trueRandom?) cos(accurate?), sin(accurate?), etc.
// TODO: Doc for API

data.set = function (key: string, value: unknown): string {
  return `${key}=${value}`
}
data.get = function (key: string): string {
  return key
}
data.log = function (...values): void {
  console.log(deepJoin(values))
}

/**
 * group with parenthesis
 * @param body
 * @returns (body)
 */
data.group = function (...body) {
  return `(${deepJoin(body)})`
}

data.add = function (...values) {
  return values.join('+')
}
/**
 * @param values
 * @returns a-b-c
 */
data.sub = function (...values) {
  return values.join('-')
}
data.mul = function (...values) {
  return values.join('*')
}
data.div = function (...values) {
  return values.join('/')
}
data.mod = function (...values) {
  return values.join('%')
}
data.random = function (max, min) {
  return `new Date%${max}` + min ? `+${min}` : ''
}
data.undefined = function (safe) {
  return safe ? `([]._)` : `[]._`
}
data.infinity = function (safe) {
  return safe ? `(1/0)` : `1/0`
}
