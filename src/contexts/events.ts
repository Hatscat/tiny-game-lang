import { Data } from 'metalthea'
import { deepJoin } from 'utils/data-structure'

export const events: Data.ExecutionContext = {}
// TODO: loop(shadowVar ?), onClick(shadowVars?), onInput(shadowVars?), etc.
events.loop = function (...body) {
  return `setInterval(e=>{${deepJoin(body)}},33);`
}

events.onClick = function (...body) {
  return `onclick(e=>{${deepJoin(body)}});`
}

events.onInput = function (...body) {
  return `oninput(e=>{${deepJoin(body)}});`
}
