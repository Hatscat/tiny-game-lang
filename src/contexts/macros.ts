import { Data } from 'metalthea'

export const macros: Data.ExecutionContext = {}

const globalData: Record<string, any> = {}

/* Arithmetic functions */

macros.add = function (...arguments_) {
  return arguments_.reduce((a, b) => a + b)
}
macros.sub = function (...arguments_) {
  return arguments_.reduce((a, b) => a - b)
}
macros.mul = function (...arguments_) {
  return arguments_.reduce((a, b) => a * b)
}
macros.div = function (...arguments_) {
  return arguments_.reduce((a, b) => a / b)
}
macros.mod = function (...arguments_) {
  return arguments_.reduce((a, b) => a % b)
}

/* Data structures */

macros.Record = function (...arguments_) {
  const record: Record<any, any> = {}
  for (let index = 0; index < arguments_.length; index += 2) {
    record[arguments_[index]] = arguments_[index + 1]
  }
  return record
}
macros.Array = function (...arguments_) {
  return [...arguments_]
}
macros.map = function (lambda) {
  // eslint-disable-next-line unicorn/no-array-callback-reference
  return (array: any[]) => array.map(lambda)
}

/* Utils */

macros.exec = function (expr) {
  return eval(expr)
}
macros.def = function (variableName, value) {
  globalData[variableName] = value

  return ''
}
macros.get = function (variableName) {
  return globalData[variableName]
}
macros.for = function (from, to, step, bodyLambda) {
  let result: any[] = []
  for (let index = from; index < to; index += step) {
    const stepResult = bodyLambda({ ...this, i: index }, index, from, to, step)
    // eslint-disable-next-line unicorn/prefer-spread
    result = result.concat(stepResult)
  }

  return result
}
macros.join = function (separator) {
  return (array: any[]) => array.join(separator || '')
}
macros.comment = function () {
  return ''
}
