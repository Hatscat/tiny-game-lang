export function shortestString(...options: string[]): string {
  return options.reduce((shortestValue, currentValue) =>
    currentValue.length < shortestValue.length ? currentValue : shortestValue
  )
}
