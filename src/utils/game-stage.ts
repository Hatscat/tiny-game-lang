export enum GameStage {
  Menu = 0,
  Play = 1,
  GameOver = 2,
}
