export function deepJoin(data: unknown, separator = ''): string {
  return Array.isArray(data)
    ? data.map((value) => deepJoin(value, separator)).join(separator)
    : String(data)
}
