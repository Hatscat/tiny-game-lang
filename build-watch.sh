#!/usr/bin/env bash

nohup yarn rollup --config --watch &

nohup metalthea 'dist/index.js' 'tests/integration/*.html' 'build' --verbose --watch &

tail -f nohup.out
