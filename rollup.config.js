import typescript from '@rollup/plugin-typescript'
import multi from '@rollup/plugin-multi-entry'

export default {
  input: ['src/contexts/*.ts'],
  output: {
    file: 'dist/index.js',
    format: 'cjs',
  },
  plugins: [multi(), typescript()],
}
