#!/usr/bin/env bash

tests_dir="tests/integration"

rm -r "$tests_dir/result";

yarn metalthea "$tests_dir/context.js" "$tests_dir/*.html" "$tests_dir/result" \
&& diff -r "$tests_dir/result" "$tests_dir/expected-result" \
&& echo 'Integration Test passed!';
