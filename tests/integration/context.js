const tinyGameContexts = require('../../dist/index.js')

const context = {}

context.test = function () {
  return 'Hello World!'
}

module.exports = {
  ...tinyGameContexts,
  ...context,
}
